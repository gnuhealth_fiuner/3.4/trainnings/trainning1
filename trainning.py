# -*- coding: utf-8 -*-
##############################################################################
#
#    GNU Health: The Free Health and Hospital Information System
#    Copyright (C) 2008-2018 Luis Falcon <lfalcon@gnusolidario.org>
#    Copyright (C) 2011-2018 GNU Solidario <health@gnusolidario.org>
#    Copyright (C) 2015 Cédric Krier
#    Copyright (C) 2014-2015 Chris Zimmerman <siv@riseup.net>
#
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from trytond.model import ModelView, ModelSQL, fields

__all__ = ['Chance']

#### All arguments on the class declaration, defines which kind heritates
#### In this case, ModelSQL gives persistence on the datadase,
####and ModelView gives declaration of viewl
class Chance(ModelSQL, ModelView):
    ####First of all, we docstring the class
    "Chance"
    ####Second, we named the table (persistent or not) related
    ####to the class
    __name__ = 'trainning.chance'
    ################################
    ################################
    #we declare all the class attributes, that will be related to 
    #database table fields
    name = fields.Char("Name", help = "Chance Name")
    chance_date = fields.DateTime("Date", help = "This chance date")
    how_many_tries = fields.Integer("How many", help="How many tries")
    is_discard =  fields.Boolean("Is this chance have been discard?")
    description = fields.Function(fields.Text('Description of the chance'),
                                  'get_description')
    states = fields.Selection([
        (None,''),
        ('draft','Draft'),
        ('in_process', 'In Process'),
        ('done','Done'),        
        ],"State", help="State of the chance")
    
    def get_description(self, name):
        if self.name and self.how_many_tries:
            return "This chance "+ self.name + " has been tried "+ str(self.how_many_tries)
        return "This chance has no tries yet"
